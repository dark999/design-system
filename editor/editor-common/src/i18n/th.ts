/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Thai
export default {
  'fabric.editor.codeBidiWarningLabel':
    'ตัวอักษรแบบสองทิศทางเปลี่ยนลำดับการแสดงข้อความ ซึ่งอาจนำไปใช้เพื่อปิดบังรหัสที่มีวัตถุประสงค์ในทางที่ไม่ดีได้',
  'fabric.editor.captionPlaceholder': 'เพิ่มคำอธิบาย',
  'fabric.editor.collapseNode': 'ย่อเนื้อหา',
  'fabric.editor.expandDefaultTitle': 'คลิกที่นี่เพื่อขยาย...',
  'fabric.editor.expandNode': 'ขยายเนื้อหา',
  'fabric.editor.expandPlaceholder': 'ตั้งชื่อให้ส่วนขยาย...',
  'fabric.editor.openLink': 'เปิดลิงก์ในแถบใหม่',
  'fabric.editor.unsupportedBlockContent':
    'โปรแกรมแก้ไขนี้ไม่รองรับการแสดงเนื้อหานี้',
  'fabric.editor.unsupportedContentTooltip':
    'เนื้อหาไม่สามารถแสดงได้ในโปรแกรมแก้ไขนี้ ซึ่งจะมีการสำรองเนื้อหาไว้เมื่อคุณทำการแก้ไขและบันทึก',
  'fabric.editor.unsupportedInlineContent': 'เนื้อหาไม่ได้รับการรองรับ',
};
