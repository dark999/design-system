export default [
  'inlineExtension',
  { props: { marks: { type: 'array', items: ['fragment'], optional: true } } },
];
