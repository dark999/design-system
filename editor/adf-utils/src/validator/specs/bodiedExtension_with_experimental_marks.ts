export default [
  'bodiedExtension',
  { props: { marks: { type: 'array', items: ['fragment'], optional: true } } },
];
