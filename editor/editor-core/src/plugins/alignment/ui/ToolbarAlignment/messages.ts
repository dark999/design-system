import { defineMessages } from 'react-intl-next';

export const messages = defineMessages({
  alignment: {
    id: 'fabric.editor.alignment',
    defaultMessage: 'Text alignment',
    description: 'Opens drop down menu of options to configure text alignment',
  },
});
