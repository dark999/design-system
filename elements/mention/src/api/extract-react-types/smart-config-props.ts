import { SmartMentionConfig } from '../SmartMentionResource';

/**
 * This exists purely for the props documentation in docs/1-in-editor.tsx
 * @param props
 * @constructor
 */
export default function Props(props: SmartMentionConfig) {
  return null;
}
