import styled from 'styled-components';
import { IconTitleWrapper } from '../IconAndTitleLayout/styled';

export const SpinnerWrapper = styled(IconTitleWrapper)`
  vertical-align: baseline;
  margin-left: 2px;
`;
