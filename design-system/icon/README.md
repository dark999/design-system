# Icon

A React package that contains icons.

## Installation

```sh
yarn add @atlaskit/icon
```

## Usage

Detailed docs and example usage can be found [here](https://atlaskit.atlassian.com/packages/design-system/icon).
For adding a new icon to the icon set please follow the [adding new icons](https://developer.atlassian.com/cloud/framework/atlassian-frontend/documentation/02-adding-new-icons/) documentation.
