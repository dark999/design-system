import type { AccentColorTokenSchema, AttributeSchema } from '../../../types';

const color: AttributeSchema<AccentColorTokenSchema> = {
  color: {
    accent: {
      boldBlue: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for blue backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags.',
        },
      },
      boldGreen: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for green backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags.',
        },
      },
      boldOrange: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for orange backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags.',
        },
      },
      boldPurple: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for purple backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags.',
        },
      },
      boldRed: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for red backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags.',
        },
      },
      boldTeal: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for teal backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags.',
        },
      },
      subtleBlue: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for blue subdued backgrounds when there is no meaning tied to the color, such as colored tags.',
        },
      },
      subtleGreen: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for green subdued backgrounds when there is no meaning tied to the color, such as colored tags.',
        },
      },
      subtleMagenta: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for magenta subdued backgrounds when there is no meaning tied to the color, such as colored tags.',
        },
      },
      subtleOrange: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for orange subdued backgrounds when there is no meaning tied to the color, such as colored tags.',
        },
      },
      subtlePurple: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for purple subdued backgrounds when there is no meaning tied to the color, such as colored tags.',
        },
      },
      subtleRed: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for red subdued backgrounds when there is no meaning tied to the color, such as colored tags.',
        },
      },
      subtleTeal: {
        attributes: {
          group: 'paint',
          state: 'active',
          description:
            'Use for teal subdued backgrounds when there is no meaning tied to the color, such as colored tags.',
        },
      },
    },
  },
};

export default color;
