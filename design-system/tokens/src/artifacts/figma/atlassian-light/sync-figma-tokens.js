
/* eslint-disable no-undef */

// THIS IS AN AUTO-GENERATED FILE DO NOT MODIFY DIRECTLY
// Re-generate by running `yarn build tokens`.
// Read the instructions to use this here:
// `packages/design-system/tokens/src/figma/README.md`
synchronizeFigmaTokens('AtlassianLight', {
  "Color/Accent/BoldBlue": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for blue backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#579DFF"
  },
  "Color/Accent/BoldGreen": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for green backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#4BCE97"
  },
  "Color/Accent/BoldOrange": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for orange backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#FAA53D"
  },
  "Color/Accent/BoldPurple": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for purple backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#9F8FEF"
  },
  "Color/Accent/BoldRed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for red backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#F87462"
  },
  "Color/Accent/BoldTeal": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for teal backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#60C6D2"
  },
  "Color/Accent/SubtleBlue": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for blue subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#CCE0FF"
  },
  "Color/Accent/SubtleGreen": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for green subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#BAF3DB"
  },
  "Color/Accent/SubtleMagenta": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for magenta subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#FDD0EC"
  },
  "Color/Accent/SubtleOrange": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for orange subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#FFE2BD"
  },
  "Color/Accent/SubtlePurple": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for purple subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#DFD8FD"
  },
  "Color/Accent/SubtleRed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for red subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#FFD2CC"
  },
  "Color/Accent/SubtleTeal": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for teal subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#C1F0F5"
  },
  "Color/Background/Sunken": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a secondary background for the UI"
    },
    "value": "#091E4208"
  },
  "Color/Background/Default": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as the primary background for the UI"
    },
    "value": "#FFFFFF"
  },
  "Color/Background/Card": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for the background of raised cards, such as Jira cards on a Kanban board.\nCombine with shadow.card."
    },
    "value": "#FFFFFF"
  },
  "Color/Background/Overlay": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for the background of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the background of raised cards in a dragged state.\n\nCombine with shadow.overlay."
    },
    "value": "#FFFFFF"
  },
  "Color/Background/Selected Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for backgrounds of elements in a selected state"
    },
    "value": "#E9F2FF"
  },
  "Color/Background/Selected Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.selected"
    },
    "value": "#CCE0FF"
  },
  "Color/Background/Selected Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.selected"
    },
    "value": "#85B8FF"
  },
  "Color/Background/Blanket": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for the screen overlay that appears with modal dialogs"
    },
    "value": "#091E427A"
  },
  "Color/Background/Disabled": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for backgrounds of elements in a disabled state"
    },
    "value": "#091E420F"
  },
  "Color/Background/BoldBrand Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like primary buttons and bold in progress lozenges."
    },
    "value": "#0C66E4"
  },
  "Color/Background/BoldBrand Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldBrand"
    },
    "value": "#0055CC"
  },
  "Color/Background/BoldBrand Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldBrand"
    },
    "value": "#09326C"
  },
  "Color/Background/SubtleBrand Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like information section messages and in progress lozenges."
    },
    "value": "#E9F2FF"
  },
  "Color/Background/SubtleBrand Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleBrand"
    },
    "value": "#CCE0FF"
  },
  "Color/Background/SubtleBrand Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleBrand"
    },
    "value": "#85B8FF"
  },
  "Color/Background/BoldDanger Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like danger buttons and bold removed lozenges."
    },
    "value": "#CA3521"
  },
  "Color/Background/BoldDanger Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldDanger"
    },
    "value": "#AE2A19"
  },
  "Color/Background/BoldDanger Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldDanger"
    },
    "value": "#601E16"
  },
  "Color/Background/SubtleDanger Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like error section messages and removed lozenges."
    },
    "value": "#FFEDEB"
  },
  "Color/Background/SubtleDanger Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleDanger"
    },
    "value": "#FFD2CC"
  },
  "Color/Background/SubtleDanger Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleDanger"
    },
    "value": "#FF9C8F"
  },
  "Color/Background/BoldWarning Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like warning buttons and bold moved lozenges."
    },
    "value": "#E2B203"
  },
  "Color/Background/BoldWarning Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldWarning"
    },
    "value": "#CF9F02"
  },
  "Color/Background/BoldWarning Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldWarning"
    },
    "value": "#B38600"
  },
  "Color/Background/SubtleWarning Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like warning section messages and moved lozenges."
    },
    "value": "#FFF7D6"
  },
  "Color/Background/SubtleWarning Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleWarning"
    },
    "value": "#F8E6A0"
  },
  "Color/Background/SubtleWarning Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleWarning"
    },
    "value": "#F5CD47"
  },
  "Color/Background/BoldSuccess Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like checked toggles and bold success lozenges."
    },
    "value": "#1F845A"
  },
  "Color/Background/BoldSuccess Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldSuccess"
    },
    "value": "#216E4E"
  },
  "Color/Background/BoldSuccess Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldSuccess"
    },
    "value": "#164B35"
  },
  "Color/Background/SubtleSuccess Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like success section messages and success lozenges. "
    },
    "value": "#DFFCF0"
  },
  "Color/Background/SubtleSuccess Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleSuccess"
    },
    "value": "#BAF3DB"
  },
  "Color/Background/SubtleSuccess Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleSuccess"
    },
    "value": "#7EE2B8"
  },
  "Color/Background/BoldDiscovery Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like onboarding buttons and bold new lozenges."
    },
    "value": "#6E5DC6"
  },
  "Color/Background/BoldDiscovery Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldDiscovery"
    },
    "value": "#5E4DB2"
  },
  "Color/Background/BoldDiscovery Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldDiscovery"
    },
    "value": "#352C63"
  },
  "Color/Background/SubtleDiscovery Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like discovery section messages and new lozenges."
    },
    "value": "#F3F0FF"
  },
  "Color/Background/SubtleDiscovery Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleDiscovery"
    },
    "value": "#DFD8FD"
  },
  "Color/Background/SubtleDiscovery Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleDiscovery"
    },
    "value": "#B8ACF6"
  },
  "Color/Background/BoldNeutral Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like unchecked toggles and bold default lozenges."
    },
    "value": "#44546F"
  },
  "Color/Background/BoldNeutral Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldNeutral"
    },
    "value": "#2C3E5D"
  },
  "Color/Background/BoldNeutral Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldNeutral"
    },
    "value": "#172B4D"
  },
  "Color/Background/TransparentNeutral Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for UIs that don’t have a default background, such as menu items or subtle buttons."
    },
    "value": "#091E420F"
  },
  "Color/Background/TransparentNeutral Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for UIs that don’t have a default background, such as menu items or subtle buttons."
    },
    "value": "#091E4224"
  },
  "Color/Background/SubtleNeutral Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as the default background of UI elements like buttons, lozenges, and tags."
    },
    "value": "#091E420F"
  },
  "Color/Background/SubtleNeutral Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleNeutral"
    },
    "value": "#091E4224"
  },
  "Color/Background/SubtleNeutral Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleNeutral"
    },
    "value": "#091E424F"
  },
  "Color/Background/SubtleBorderedNeutral Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleBorderedNeutral"
    },
    "value": "#091E4208"
  },
  "Color/Background/SubtleBorderedNeutral Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleBorderedNeutral"
    },
    "value": "#091E420F"
  },
  "Color/Border/Focus": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for focus rings of elements in a focus state"
    },
    "value": "#388BFF"
  },
  "Color/Border/Neutral": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use to create borders around UI elements such as text fields, checkboxes, and radio buttons, or to visually group or separate UI elements, such as flat cards or side panel dividers"
    },
    "value": "#091E4224"
  },
  "Color/IconBorder/Brand": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons or borders representing brand, in-progress, or information, such as the icons in information sections messages.\n\nAlso use for blue icons or borders when there is no meaning tied to the color, such as file type icons."
    },
    "value": "#1D7AFC"
  },
  "Color/IconBorder/Danger": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing critical information, such the icons in error section messages or the borders on invalid text fields.\n\nAlso use for red icons or borders when there is no meaning tied to the color, such as file type icons."
    },
    "value": "#E34935"
  },
  "Color/IconBorder/Warning": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing semi-urgent information, such as the icons in warning section messages.\n\nAlso use for yellow icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#D97008"
  },
  "Color/IconBorder/Success": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing positive information, such as the icons in success section messages or the borders on validated text fields.\n\nAlso use for green icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#22A06B"
  },
  "Color/IconBorder/Discovery": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing new information, such as the icons in discovery section mesages or the borders in onboarding spotlights.\n\nAlso use for purple icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#8270DB"
  },
  "Color/Overlay Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a background overlay for elements in a hover state when their background color cannot change, such as avatars."
    },
    "value": "#091E424F"
  },
  "Color/Overlay Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a background overlay for elements in a pressed state when their background color cannot change, such as avatars."
    },
    "value": "#091E427A"
  },
  "Color/Text/Selected": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text, icons, borders, or other visual indicators in selected states"
    },
    "value": "#0C66E4"
  },
  "Color/Text/HighEmphasis": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for primary text, such as body copy, sentence case headers, and buttons"
    },
    "value": "#172B4D"
  },
  "Color/Text/MediumEmphasis": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for secondary text, such navigation, subtle button links, input field labels, and all caps subheadings.\n\nUse for icon-only buttons, or icons paired with text.highEmphasis text\n      "
    },
    "value": "#44546F"
  },
  "Color/Text/LowEmphasis": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for tertiary text, such as meta-data, breadcrumbs, input field placeholder and helper text.\n\nUse for icons that are paired with text.medEmphasis text"
    },
    "value": "#626F86"
  },
  "Color/Text/OnBold": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons when on bold backgrounds"
    },
    "value": "#FFFFFF"
  },
  "Color/Text/OnBoldWarning": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons when on bold warning backgrounds"
    },
    "value": "#172B4D"
  },
  "Color/Text/Link Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for links in a resting or hover state. Add an underline for hover states"
    },
    "value": "#0C66E4"
  },
  "Color/Text/Link Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for links in a pressed state"
    },
    "value": "#0055CC"
  },
  "Color/Text/Brand": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle brand backgrounds, such as in progress lozenges, or on subtle blue accent backgrounds, such as colored tags."
    },
    "value": "#0055CC"
  },
  "Color/Text/Warning": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle warning backgrounds, such as in lozenges, or text on subtle warning backgrounds, such as in moved lozenges"
    },
    "value": "#974F0C"
  },
  "Color/Text/Danger": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for critical text, such as input field error messaging, or text on subtle danger backgrounds, such as in removed lozenges, or text on subtle red accent backgrounds, such as colored tags."
    },
    "value": "#AE2A19"
  },
  "Color/Text/Success": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for positive text, such as input field success messaging, or text on subtle success backgrounds, such as in success lozenges, or text on subtle green accent backgrounds, such as colored tags."
    },
    "value": "#216E4E"
  },
  "Color/Text/Discovery": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle discovery backgrounds, such as in new lozenges, or text on subtle purple accent backgrounds, such as colored tags."
    },
    "value": "#5E4DB2"
  },
  "Color/Text/Disabled": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons in disabled states"
    },
    "value": "#8993A5"
  },
  "Shadow/Card": {
    "attributes": {
      "group": "shadow",
      "state": "active",
      "description": "\nUse for the box shadow of raised card elements, such as Jira cards on a Kanban board.\n\nCombine with background.overlay"
    },
    "value": [
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 1
        },
        "color": "#091E42",
        "opacity": 0.25
      },
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 0
        },
        "color": "#091E42",
        "opacity": 0.31
      }
    ]
  },
  "Shadow/Overlay": {
    "attributes": {
      "group": "shadow",
      "state": "active",
      "description": "\nUse for the box shadow of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the box shadow of raised cards in a dragged state.\n\nCombine with background.overlay"
    },
    "value": [
      {
        "radius": 12,
        "offset": {
          "x": 0,
          "y": 8
        },
        "color": "#091E42",
        "opacity": 0.15
      },
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 0
        },
        "color": "#091E42",
        "opacity": 0.31
      }
    ]
  }
}, {});
